# Polynomials Assignment

This is your base repository for solving the `Polynom` task.

## Eclipse project

This repository contains `.project` file that can be used to import
this project into Eclipse (*File -> Import*).

## Assignment One

 1. Finish implementation of `Main.printHelp` (do print some useful
    information there).
 2. Implement all the non-static methods inside `Polynomial` so that the
    tests in `PolynomialTest` pass.

