
/** Integer-only polynomials. */
public class Polynomial {

	/**
	 * Create new instance with given coefficients.
	 *
	 * x^2 + 3x - 7 would be created by new Polynomial(-7, 3, 1) x^5 + 42 would be
	 * created by new Polynomial(42, 0, 0, 0, 0, 1)
	 *
	 * @param coef Coefficients, ordered from lowest degree (power).
	 */

	int[] koef;

	public Polynomial(int... coef) {
		System.out.printf("Creating polynomial");
		for (int i = 0; i < coef.length; i++) {
			if (i > 0) {
				System.out.print(" +");
			}
			System.out.printf(" %d * x^%d", coef[i], i);
		}
		System.out.println(" ...");

		koef = coef;
	}

	/** Get coefficient value for given degree (power). */
	public int getCoefficient(int deg) {
		int koeficient = 0;

		if (deg < koef.length) {
			koeficient = koef[deg];
		}

		return koeficient;
	}

	/** Get degree (max used power) of the polynomial. */
	public int getDegree() {
		int stupen = 0;

		for (int i = koef.length - 1; i >= 0; i--) {
			if (koef[i] != 0) {
				stupen = i;
				/*if (i == 0) {
					stupen = 1;
				}*/
				break;
			}
		}

		return stupen;
	}

	/** Format into human-readable form with given variable. */
	public String toPrettyString(String variable) {
		String vystup = "";

		Boolean zapis = false;

		for (int i = koef.length - 1; i >= 0; i--) {

			if (koef[i] != 0) {

				if (i < koef.length - 1 && zapis == true) {
					if (koef[i] >= 0) {
						vystup += " + ";
					} else if (koef[i] < 0) {
						vystup += " - ";
					}
				}

				zapis = false;

				if (koef[i] != 1 || i == 0) {
					if (koef[i] < 0 && i < koef.length - 1) {
						vystup += koef[i] * (-1);
					} else {
						vystup += koef[i];
					}
					zapis = true;
				}

				if (i > 0) {
					vystup += variable;
					zapis = true;
				}

				if (i > 1) {
					vystup += "^";
					vystup += i;
				}

			}

		}

		return vystup;
	}

	/** Debugging output, dump only coefficients. */
	@Override
	public String toString() {
		String vystup = "Polynomial[";

		for (int i = 0; i < koef.length; i++) {
			if (i > 0) {
				vystup += ",";
			}
			vystup += koef[i];
		}

		vystup += "]";

		return vystup;
	}

	/** Adds together given polynomials, returning new one. */
	public static Polynomial sum(Polynomial... polynomials) {
		int resultDegree = 0;
		for (int i = 0; i < polynomials.length; i++) {
			if (polynomials[i].getDegree() > resultDegree) {
				resultDegree = polynomials[i].getDegree();
			}
		}
		int[] resultCoef = new int[resultDegree+1];
		for (int i = 0; i < polynomials.length; i++) {
			for (int j = 0; j <= polynomials[i].getDegree(); j++) {
				resultCoef[j] += polynomials[i].getCoefficient(j);
			}
		}
		
		Polynomial result = new Polynomial(resultCoef);
		return result;
	}

	/** Multiplies together given polynomials, returning new one. */
	public static Polynomial product(Polynomial... polynomials) {
		return new Polynomial(0);
	}

	/** Get the result of division of two polynomials, ignoring remainder. */
	public static Polynomial div(Polynomial dividend, Polynomial divisor) {
		return new Polynomial(0);
	}

	/** Get the remainder of division of two polynomials. */
	public static Polynomial mod(Polynomial dividend, Polynomial divisor) {
		return new Polynomial(0);
	}
}
